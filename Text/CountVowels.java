/*
 * Taylor Caldwell
 * CountVowels.java
 * 
 * Count Vowels - Enter a string and the 
 * program counts the number of vowels in 
 * the text. For added complexity have it 
 * report a sum of each vowel found.
 */

import java.util.Scanner;

public class CountVowels {
	
		static int countA, countE, countI, countO, countU, total;
		
        public static void main(String[] args) {
        		
                Scanner scan = new Scanner(System.in);
                System.out.println("Insert a sentence:");
                String str = scan.nextLine().toUpperCase();
                scan.close();
                
                for(int i = 0; i < str.length(); i++){
                	if(str.charAt(i) == 'A'){countA++; total++;}
                	else if(str.charAt(i) == 'E'){countE++; total++;}
                	else if(str.charAt(i) == 'I'){countI++; total++;}
                	else if(str.charAt(i) == 'O'){countO++; total++;}
                	else if(str.charAt(i) == 'U'){countU++; total++;}
                	else{}
                }
                
                System.out.println("Number of A's: " + countA);
                System.out.println("Number of E's: " + countE);
                System.out.println("Number of I's: " + countI);
                System.out.println("Number of O's: " + countO);
                System.out.println("Number of U's: " + countU);
                System.out.println("Total number of vowels: " + total);

        }

}
