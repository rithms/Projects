/*
 * Taylor Caldwell
 * reverse_string.java
 * Reverse a String - Enter a string and the 
 * program will reverse it and print it out.
 */

import java.util.Scanner;

public class reverse_string {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Insert the String you would like to reverse:");
		String str = scan.nextLine();
		scan.close();
		String reversedStr = new StringBuilder(str).reverse().toString();
		System.out.println("Initial String: " + str);
		System.out.println("Reversed String: " + reversedStr);

	}

}
