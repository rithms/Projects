/*
 * Taylor Caldwell
 * PigLatin.java
 * 
 * Pig Latin - Pig Latin is a game of alterations played on the English language game.
 *  To create the Pig Latin form of an English word the initial consonant sound is 
 *  transposed to the end of the word and an ay is affixed 
 *  (Ex.: "banana" would yield anana-bay). Read Wikipedia 
 *  for more information on rules.
 */

import java.util.Scanner;

public class PigLatin {

        public static void main(String[] args) {
        		
        		String pig = "";
        		char[] vowels = {'A', 'E', 'I', 'O', 'U'}; // this program assumes 'y' is always used as a constant
                Scanner scan = new Scanner(System.in);
                System.out.println("Insert a word to be translated to Pig Latin:");
                String str = scan.nextLine();
                scan.close();
                
                for(int i = 0; i < vowels.length; i++){
                	if(str.toUpperCase().charAt(0) == vowels[i]){
                		pig = str.substring(0,1).toUpperCase() + str.substring(1).toLowerCase() + "ay";
                		break;
                	}
                }
                
                if(pig.isEmpty()){pig = str.substring(1,2).toUpperCase() + str.substring(2).toLowerCase() + str.toLowerCase().charAt(0) + "ay";}
                
                System.out.println("Original Word: " + str);
                System.out.println("Pig Latin Word: " + pig);

        }

}
